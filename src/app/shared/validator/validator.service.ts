import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  public nombreApellidoPattern: string='([a-zA-Z]+) ([a-zA-Z]+)'
  public emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";


  constructor() { }

  noPuedeSer(argumento: FormControl): ValidationErrors | null{
    console.log("arg", argumento.value);
    const valor: string = argumento.value?.trim().toLowerCase();
    if(valor === 'strider'){
      return {
        noStrider: true //objeto y true cuando se encontro error
      }
    }

    return null; //nulo cuando no se tiene errores
  } 

  camposIguales(campo1: string, campo2: string){
    return (formGroup: AbstractControl): ValidationErrors | null =>{
      //console.log("camposIguales", formGroup);
      const pass1 = formGroup.get(campo1)?.value;
      const pass2 = formGroup.get(campo2)?.value;
      if(pass1 !== pass2){
        formGroup.get(campo2)?.setErrors({ noIguales: true})
        return {noIguales: true}
      }

      formGroup.get(campo2)?.setErrors(null);
      return null;
    }
  }
  
}
