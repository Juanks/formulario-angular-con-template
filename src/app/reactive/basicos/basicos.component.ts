import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {
//se reemplaza con formBuilder
 /*  miFormulario: FormGroup = new FormGroup({
    'nombre': new FormControl('RTX 77123'),
    'precio': new FormControl(100),
    'existencia': new FormControl(5),
  }); */

  miFormulario: FormGroup = this._fb.group({
    nombre: [, /**validador sincrono */ [Validators.required, Validators.minLength(3)]],
    precio: [ , [Validators.required, Validators.min(0)] ],
    existencia: [, [Validators.required, Validators.min(0)] ],
  })

  constructor(private _fb: FormBuilder) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.miFormulario.setValue({
      nombre: 'RADION',
      precio: 12312,
      existencia:234
    })

    this.miFormulario.reset({
      nombre: 'RADION',
      precio: 12312,
      //existencia:234
    })
  }

  /* campoEsValido(){
    return this.miFormulario.controls['nombre'].errors
            && this.miFormulario.controls['nombre'].touched
  } */
  campoEsValido(campo: string){
    return this.miFormulario.controls[campo].errors
            && this.miFormulario.controls[campo].touched
  }

  guardar(){
    if(this.miFormulario.invalid){
      this.miFormulario.markAllAsTouched();
      return;
    }
    console.log(this.miFormulario.value);
    this.miFormulario.reset();
  }
}
