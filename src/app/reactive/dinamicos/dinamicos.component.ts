import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent implements OnInit {

  miFormulario : FormGroup = this.fb.group({
    nombre: [,[Validators.required, Validators.minLength(3)]],
    favoritos: this.fb.array([ //arreglo
      ['Metal',Validators.required], //form controls
      ['Slug', Validators.required],  //form controls
    ], Validators.required)

  })
//capturar datos de un campo o control = input, button, etc
  nuevoFavorito: FormControl = this.fb.control('', Validators.required);

  get favoritosArr(){
    return this.miFormulario.get('favoritos') as FormArray;
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  campoEsValido(campo: string){
    return this.miFormulario.controls[campo].errors
            && this.miFormulario.controls[campo].touched
  }

  agregarFav(){
    if(this.nuevoFavorito.invalid){
      console.log("no tiene dato");
      return;
    }
    
    //this.favoritosArr.push( new FormControl(this.nuevoFavorito.value, Validators.required));
    // OOOO
    this.favoritosArr.push( this.fb.control(this.nuevoFavorito.value, Validators.required));
    this.nuevoFavorito.reset();

  }

  guardar (){
    if(this.miFormulario.invalid){
      this.miFormulario.markAllAsTouched()
      return;
    }
    console.log("mi form");
  }

  eliminar(index: number){
    this.favoritosArr.removeAt(index)
  }
}
