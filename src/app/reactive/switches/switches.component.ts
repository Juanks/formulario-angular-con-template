import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [
  ]
})
export class SwitchesComponent implements OnInit {

  
  miFormulario: FormGroup = this.fb.group({
    genero:['M', Validators.required],
    notificaciones:[false, Validators.required],
    condiciones: [false, Validators.requiredTrue]
  })

  persona ={
    genero: 'F',
    notificaciones: true,
    edad:34
  }


  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    //this.miFormulario.setValue( this.persona )
    //OOO
    this.miFormulario.reset( {...this.persona, condiciones: false} )

    /* this.miFormulario.valueChanges.subscribe( form =>{
      console.log("FORNM",form);
      delete form.condiciones;
      this.persona = form;
    })
 */
//oo
    this.miFormulario.valueChanges.subscribe( ({ condiciones, ...restoDeArgumentos}) =>{
      console.log("FORNM rest",restoDeArgumentos);
      this.persona = restoDeArgumentos;
    })
  }

  guardar(){
    const formValue = {...this.miFormulario.value};
    //delete formValue.condiciones
    console.log(formValue);
    this.persona = formValue;
  }

}
