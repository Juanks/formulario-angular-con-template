import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { nombreApellidoPattern, emailPattern, noPuedeSer } from '../../../shared/validator/validaciones';
import { ValidatorService } from '../../../shared/validator/validator.service';
import { EmailValidatorService } from '../../../shared/validator/email-validator.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styles: [
  ]
})
export class RegistroComponent implements OnInit {

/*   nombreApellidoPattern: string='([a-zA-Z]+) ([a-zA-Z]+)'
  emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"; */

/*   noPuedeSer(argumento: FormControl){
    console.log("arg", argumento.value);
    const valor: string = argumento.value?.trim().toLowerCase();
    if(valor === 'strider'){
      return {
        noStrider: true //objeto y true cuando se encontro error
      }
    }

    return null; //nulo cuando no se tiene errores
  } */

  /* miFormulario : FormGroup = this.fb.group({
    nombre: ['',[Validators.required, Validators.pattern(nombreApellidoPattern)]],
    email: ['',[Validators.required, Validators.pattern(emailPattern)]],
    userName: ['',[Validators.required, noPuedeSer]],
  }) */

  miFormulario : FormGroup = this.fb.group({
    nombre: ['',[Validators.required, Validators.pattern(this._validatorService.nombreApellidoPattern)]],
    email: ['',[Validators.required, Validators.pattern(this._validatorService.emailPattern)], [this._emailValidator]],
    userName: ['',[Validators.required, this._validatorService.noPuedeSer]],
    password: ['',[Validators.required, Validators.minLength(6)]],
    password2: ['',[Validators.required, ]],

  },{
    validators: [this._validatorService.camposIguales('password', 'password2')]
  })

  constructor(private fb: FormBuilder,
              private _validatorService: ValidatorService,
              private _emailValidator: EmailValidatorService) { }

  ngOnInit(): void {
    this.miFormulario.reset({
      nombre: 'Lucas Film',
      email: 'test1@test.com',
      userName: 'ala',
      password: '123456',
      password2: '123456'
    })
  }

  campoNoValido(campo: string){
    return this.miFormulario.get(campo)?.invalid
          && this.miFormulario.get(campo)?.touched;
  }

  emailRequerido(){
    return this.miFormulario.get('email')?.errors?.['required']
          && this.miFormulario.get('email')?.touched;
  }

  emailFormato(){
    return this.miFormulario.get('email')?.errors?.['pattern']
          && this.miFormulario.get('email')?.touched;
  }
  emailTomado(){
    return this.miFormulario.get('email')?.errors?.['emailTomado1']
          && this.miFormulario.get('email')?.touched;
  }
  
  submitFormulario(){
    console.log(this.miFormulario);
    this.miFormulario.markAllAsTouched();
  }
}
