import { Component, OnInit, ViewChild } from '@angular/core';

interface Persona{
  nombre: string,
  favoritos: Favorito[];
}

interface  Favorito{
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent implements OnInit {

  nuevoJuego: string ='';

  persona: Persona={
    nombre: 'Luck',
    favoritos:[
      { id: 1,nombre:'Metal Hero'},
      { id: 2,nombre:'Metal'},
    ]
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar(){
    console.log("asdf");
  }

  eliminar(index: number){
    this.persona.favoritos.splice(index, 1)
  }

  agregar(){
    const nuevoFavorito: Favorito={
      id: this.persona.favoritos.length+1,
      nombre: this.nuevoJuego,
    };

    this.persona.favoritos.push({...nuevoFavorito}) //no permite enviar referenciar al objeto
    this.nuevoJuego = ''
  }
}
