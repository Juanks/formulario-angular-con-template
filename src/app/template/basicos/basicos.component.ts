import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {
  @ViewChild('miFormulario') miFormulario!: NgForm;

  initForm ={
    producto:'RT100',
    precio: 10,
    existencias:50
  }

  constructor() { }

  ngOnInit(): void {
  }

 /*  guardar(miFormulario: NgForm){
    console.log("guardar!!", miFormulario.value);
  } */

  nombreValido():boolean{
    return this.miFormulario?.controls['producto']?.invalid &&
    this.miFormulario?.controls['producto']?.touched
  }

  precioValido():boolean{
    return this.miFormulario?.controls['precio']?.touched
      && this.miFormulario?.controls['precio']?.value < 0;
  }
  guardar(){
    /* console.log(this.miFormulario);
    if( this.miFormulario.controls['precio'].value < 0){
      console.log("no posteado");
      return;
    } */

    console.log('posteo ok');
    this.miFormulario.resetForm(
      {
        precio:0,
        existencias:0
      }
    );
  }

}
